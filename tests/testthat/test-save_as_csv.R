# WARNING - Generated by {fusen} from /dev/flat_study_squirrels.Rmd: do not edit by hand

test_that("save_as_csv works", {
  
  # Everything is ok
  my_temp_folder <- tempfile(pattern = "savecsv")
  dir.create(my_temp_folder)

  save_as_csv(iris, 
            path = file.path(my_temp_folder, "my_filtered_activities.csv"), col.names = NA)
  
  expect_true(file.exists(file.path(my_temp_folder, "my_filtered_activities.csv")))
  
  expect_error(iris %>% save_as_csv(file.path(my_temp_folder, "output.csv")), 
              regexp = NA)
  
  expect_error(iris %>% save_as_csv(file.path(my_temp_folder, "output.csv")) %>% browseURL(), 
               regexp = NA)
  
  unlink(my_temp_folder, recursive = TRUE)
  
  # Error
  expect_error(iris %>% save_as_csv("output.xlsx"), 
               regexp = "does not have extension csv")
  
  expect_error(NULL %>% save_as_csv("output.csv"),
               regexp = "data is not a data frame")
  
  expect_error(iris %>% save_as_csv("does/no/exist.csv"), 
               regexp = "does not exist")
  
})
