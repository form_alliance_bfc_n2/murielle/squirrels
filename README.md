
<!-- README.md is generated from README.Rmd. Please edit that file -->

# squirrels <img src="man/figures/squirrels_hex.png" align="right" alt="" width="120" />

<!-- badges: start -->
<!-- badges: end -->

The goal of squirrels is to analysis the squirrels of Central Park. Ce
package a été créé dans le cadre d’une formation N2

## Installation

You can install the development version of squirrels like so:

``` r
remotes::install.....
```

TEST

## Example

This is a basic example which shows you how to solve a common problem:

``` r
library(squirrels)
## basic example code
```

Les principales fonctionnalités sont:

``` r
get_message_fur_color(primary_fur_color = "Black")
#> We will focus on Black squirrels
```
