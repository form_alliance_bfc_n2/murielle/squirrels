#' data_act_squirrels
#'
#' Un jeu de donnees d examples sur les ecureuils
#'
#' @format A data frame with 15 rows and 4 variables:
#' \describe{
#'   \item{ age }{  character }
#'   \item{ primary_fur_color }{  character }
#'   \item{ activity }{  character }
#'   \item{ counts }{  numeric }
#' }
#' @source Source
"data_act_squirrels"
